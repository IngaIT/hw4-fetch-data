showFilms()

function showFilms() {
    const films = document.querySelector('.films');
    fetch('https://ajax.test-danit.com/api/swapi/films')
    .then((res) => res.json())
    .then((data) => {
        data.forEach(film => {
            const filmHtml = createFilm(film.name, film.episodeId, film.openingCrawl);
            films.append(filmHtml);
            const charactersHtml = filmHtml.querySelector('.characters');
            charactersHtml.classList.add('loader')
            Promise.all(film.characters.map(url => fetch(url)))
            .then(responses =>
                Promise.all(responses.map(res => res.json()))
            ).then(texts => {
                createCharacter(texts, charactersHtml)
            })
        })
    })
}

function createCharacter(arrFromPromise, arrWherePut) {
    arrWherePut.classList.remove('loader');
    arrFromPromise.forEach(elem => {
        arrWherePut.insertAdjacentHTML('beforeend', `<li>${elem.name}</li>`)
    })
}

function createFilm(name, episodeId, openingCrawl) {
    const filmHtml = document.createElement('div');
    filmHtml.classList.add('film');
    filmHtml.insertAdjacentHTML('afterbegin', `<div class="episode-id"> Episode ${episodeId}</div><div class="film-name">${name}</div><ul class="characters"></ul><div class="descr">${openingCrawl}</div>`);
    return filmHtml
}
